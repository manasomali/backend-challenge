import { PrismaClient } from '@prisma/client';
import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';
import NasaAPI from './src/dataRequest.js';

const prisma = new PrismaClient();

const typeDefs = `#graphql
  type Planet {
    pl_name: String!
    hostname: String!
    pl_bmassj: Float!
    disc_year: Int!
    has_station: Boolean
    stations_name: [String]
  }

  input StationInput {
    pl_name: String!
    station_name: String!
    user_name: String!
  }

  type Station {
    pl_name: String!
    station_name: String!
    user_name: String!
    recharging: String
    start_charge: String
    end_charge: String
  }

  type Response {
    success: Boolean!
    msg: String!
  }

  type User {
    user_name: String!
    stations_name: [String]
    has_ongoing_charge: Boolean
  }

  type Query {
    suitablePlanets: [Planet]!
    planets: [Planet]!
    stations: [Station]!
    users: [User]!
  }

  type Mutation {
    installStation(station: StationInput!): Response!
    recharge(station: StationInput!, datetime: String): Response!
  }
`;

const resolvers = {
  Query: {
    suitablePlanets: async (_, __, { dataSources }) => {
      try {
        const suitablePlanetsList = await dataSources.nasaAPI.getSuitablePlanets();
        await prisma.planet.createMany({
          data: suitablePlanetsList
        })
        return suitablePlanetsList;
      } catch (error) {
        console.error(error);
        return {
          "success": false,
          "msg": `Erro inesperado: ${error}`
        }
      }
      
    },
    planets: async () => { return await prisma.planet.findMany() },
    stations: async () => { return await prisma.station.findMany() },
    users: async () => { return await prisma.user.findMany() }
  },
  Mutation: {
    installStation: async (_, {station}) => {
      try {
        if(!(await prisma.planet.count({
          where: {
            pl_name: station.pl_name
        }}))) {
          return {
            "success": false,
            "msg": `O ${station.pl_name} planeta não existe`
          }
        }
        if(await prisma.station.count({
          where: {
            station_name: station.station_name,
          }
        })) {
          return {
            "success": false,
            "msg": `Estação com nome ${station.station_name} já existe`
          }
        }
        await prisma.planet.update({
          where: {
              pl_name: station.pl_name
          },
          data: {
            has_station: true,
            stations_name: {
              push: station.station_name
            }
          },
        })
        if(await prisma.user.count({
          where: {
            user_name: station.user_name
          }
        })) {
          await prisma.user.update({
            where: {
              user_name: station.user_name,
            },
            data: {
              stations_name: {
                push: station.station_name
              }
            }
          })
        } else {
          await prisma.user.create({
            data: {
              user_name: station.user_name,
              stations_name: [station.station_name],
              has_ongoing_charge: false
            }
          })
        }
        await prisma.station.create({
          data: {
            pl_name: station.pl_name,
            user_name: station.user_name,
            station_name: station.station_name
          }
        })
        return {
          "success": true,
          "msg": `Estação ${station.station_name} instalada com sucesso`
        }
      } catch (error) {
        console.error(error);
        return {
          "success": false,
          "msg": `Erro inesperado: ${error}`
        }
      }
    },
    recharge: async (_, {station, datetime}) => {
      try {
        if(!(await prisma.station.count({
          where: {
            station_name: station.station_name
          }
        }))) {
          return {
            "success": false,
            "msg": `Estação ${station.station_name} não instalada`
          }
        }
        if(isNaN(new Date(datetime))) {
          return {
            "success": false,
            "msg": `Data ${datetime} informato inválido`
          }
        }

        
        
        
        
        
        if(parseInt(Date.now().toString())>parseInt(Date.parse(datetime).toString())) {
          return {
            "success": false,
            "msg": `Data ${datetime} precisa ser no futuro`
          }
        }
        let stationTarget = await prisma.station.findUnique({
          where: {
            station_name: station.station_name
          }
        })
        if(stationTarget.pl_name!=station.pl_name) {
          return {
            "success": false,
            "msg": `Planeta ${station.pl_name} informado não tem essa estação`
          }
        }
        if(stationTarget.user_name!=station.user_name) {
          return {
            "success": false,
            "msg": `Usuário ${station.user_name} informado não tem essa estação`
          }
        }
        
        if(stationTarget.end_charge) {
          
          
          
          if(parseInt(Date.now().toString())<parseInt(stationTarget.end_charge)) {
            return {
              "success": false,
              "msg": `Estação ${station.station_name} já está carregando`
            }
          }
        }
        let user_data =  await prisma.user.findUnique({
          where: {
            user_name: station.user_name
          }
        })
        
        let check_station = {};
        for (let name_station of user_data.stations_name) {
          check_station = await prisma.station.findUnique({
            where: {
              station_name: name_station,
            },
          })
          if(check_station.recharging) {
            if(parseInt(Date.now().toString())<parseInt(check_station.end_charge)) {
              return {
                "success": false,
                "msg": `User ${station.user_name} já possui estação carregando`
              }
            }
          }
        }
        await prisma.station.update({
          where: {
            id: stationTarget.id,
          },
          data: {
            recharging: true,
            start_charge: Date.now().toString(),
            end_charge: Date.parse(datetime).toString()
          },
        })
        await prisma.user.update({
          where: {
            user_name: station.user_name
          },
          data: {
            has_ongoing_charge: true
          }
        })
        return {
          "success": true,
          "msg": `Estação carregando até ${datetime}`
        }
      } catch (error) {
        console.error(error);
        return {
          "success": false,
          "msg": `Erro inesperado: ${error}`
        }
      }
    }
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

const { url } = await startStandaloneServer(server, {
  context: async () => {
    const { cache } = server;
    return {
      dataSources: {
        nasaAPI: new NasaAPI({ cache }),
      },
    };
  },
});

console.log(`🚀  Server ready at ${url}`);
