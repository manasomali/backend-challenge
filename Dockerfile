FROM node:16

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY COPY . .

EXPOSE 8080

CMD [ "node", "index.ts" ]
