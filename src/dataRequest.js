import { RESTDataSource } from '@apollo/datasource-rest';

class NasaAPI extends RESTDataSource {
  baseURL = 'https://exoplanetarchive.ipac.caltech.edu/';

  async getSuitablePlanets() {
    return this.get(`TAP/sync?query=select+pl_name,hostname,pl_bmassj,disc_year+from+ps+where+pl_bmassj>10&format=JSON`);
  }
}

export default NasaAPI;