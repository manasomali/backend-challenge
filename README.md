# Backend Challenge

[Desafio proposto](#Backend Challenge Proposal). Considerações/avisos/melhorias:

- Optei por usar as tecnologias recomendadas (Apollo e Prisma) apesar de nunca ter trabalhado com elas e tive uma boa surpresa, boa documentação e fácil utilização.
- Comecei usando ts mas tive alguns problemas bestas (noob) e migrei para o bom e velho js.
- Entendo que a estrutura do projeto poderia estar bem mais organizada, está quase tudo no index.js (desculpe).
- Uso de docker somente para o mongo (seria melhor adicionar um serviço paro apollo e outro paro prisma também).
- Usuário criado quando pede para instalar uma estação (falta autenticação).
- Testes não implementados (sem tempo). Comecei a desenvolver esse challenge do sábado. Então com toda certeza tem falhas nas logica (desculpe).
- [Documentação do Apollo](http://localhost:4000/) e "Schema" na aba lateral (de preferência use o tema escuro).
- Faltou Mutation para deletar documentos (se necessário).
- Datetime como String ISO 8601(https://en.wikipedia.org/wiki/ISO_8601)

# Instruções

Para instalar as dependencias do projeto:
´´´
npm install
´´´

Para gerar o cliente prisma:
´´´
npx prisma generate
´´´

Para iniciar o servidor:
´´´
npm start
´´´

## Ajuda
- [Apollo](https://www.apollographql.com/docs/apollo-server/getting-started)
- [Prisma](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases-typescript-postgres)
- [Docker](https://docs.docker.com/compose/gettingstarted/)

## Testes

1. Query suitablePlanets
2. Query planets stations users
3. Mutation installStation em planeta que não existe
4. Mutation installStation
5. Mutation installStation com nome igual
6. Query planets stations users
7. Mutation recharge em estação que não existe
8. Mutation recharge em estação com data no formato invalido
9. Mutation recharge em estação com data no passado
10. Mutation recharge em estação com user que nao existe
11. Mutation recharge
12. Query planets stations users
13. Mutation recharge em estação carregando
14. Mutation recharge nova
15. Mutation recharge com usuário que possui estação carregando 

´´´
## Docker

Para baixar a imagem (prima requer de uma replica set):
```
docker pull prismagraphql/mongo-single-replica:4.4.3-bionic
```

Para construir a imagem:
```
docker compose build
```

Para criar a imagem:
```
docker compose up
```

Para acabar com determinado container:
```
docker kill <container id>
```

## Schema
´´´
type Planet {
  pl_name: String!
  hostname: String!
  pl_bmassj: Float!
  disc_year: Int!
  has_station: Boolean
  stations_name: [String]
}

input StationInput {
  pl_name: String!
  station_name: String!
  user_name: String!
}

type Station {
  pl_name: String!
  station_name: String!
  user_name: String!
  recharging: String
  start_charge: String
  end_charge: String
}

type Response {
  success: Boolean!
  msg: String!
}

type User {
  user_name: String!
  stations_name: [String]
  has_ongoing_charge: Boolean
}

type Query {
  suitablePlanets: Response!
  planets: [Planet!]!
  stations: [Station]!
  users: [User]!
}

type Mutation {
  installStation(station: StationInput!): Response!
  recharge(station: StationInput!, datetime: String): Response!
  reservation(station_name: String!, datetime: String!): Response!
}

# Minimal Work Check

**O seu trabalho é**:
- [x] descobrir em quais planetas a Voltbras pode instalar seus novos postos de carregamento
- [x] otimizar a experiência de recarga para os viajantes espaciais.


# Backend Challenge Proposal

O ano é 2118, 100 anos após a fundação da [Voltbras]. Expandimos nosso negócios para gerenciamento de carregamento de veículos espaciais não tripulados com propulsão de íons.
O Propulsor de Íons é um dos diversos tipos de propulsão espacial, que utiliza feixes de luz à base de energia elétrica (aí onde entra a Voltbras, iremos fornecer esta energia).

Especificamente, esta propulsão de energia deve ser provinda de combustível nuclear, pois a força de impulsão é muito forte.
Se a inserção do combustível for realizada num planeta de baixa gravidade, acontece a fissão do combustível nuclear e perde-se bastante potencial energético.
Por isso precisamos realizar o abastecimento das naves em planetas com alta gravidade, nos quais chega a ser 100 vezes mais eficiente o abastecimento.

**O seu trabalho é** descobrir em quais planetas a Voltbras pode instalar seus novos postos de carregamento e otimizar a experiência de recarga para os viajantes espaciais.

Para isso:

- utilize a API de exoplanetas da [NASA], na qual você pode consultar sua [documentação](https://exoplanetarchive.ipac.caltech.edu/docs/program_interfaces.html) e [queries comuns](https://exoplanetarchive.ipac.caltech.edu/docs/API_queries.html), o que te possibilita buscar os planetas fora do sistema solar!
- só mostre os planetas com gravidade alta, os dados não mostram exatamente qual gravidade o planeta tem, mas a Voltbras fez os cálculos e os planetas ideais(com gravidade alta), são aproximadamente os mesmos que têm sua massa maior que 10 jupiter mass (`exoplanet.pl_bmassj`)

## Requisitos

Sinta-se livre para fazer qualquer um dos próximos requisitos diferente do que foi pedido desde que consiga justificar a mudança. Ex.: não fiz o requisito de tal maneira pois a implementação que eu fiz é mais performática e segura.

- [x] Crie um servidor em Node.js usando [Apollo GraphQL Server](https://www.apollographql.com/docs/apollo-server/)
  - [x] Crie o schema GraphQL com uma query `suitablePlanets`, que retorna os dados dos planetas com gravidade alta
  - [x] Crie uma mutation `installStation`, que dado um planeta, instala uma estação de carregamento no planeta(é sugerido criar uma tabela em algum DB que guarde a informação de aonde estão instaladas as estações)
  - [x] Crie uma query `stations`, que irá listar todas as estações instaladas nos planetas
  - [x] Crie uma mutation `recharge`, que dado uma estação e um datetime de quanto a recarga irá finalizar, realiza uma recarga, começando a partir do momento em que a mutation foi chamada e finalizando com a datetime passada.
    - Só é possível realizar uma recarga na estação por vez
    - Essa recarga deve estar atrelado a um usuário - sinta-se livre para implementar da maneira que você desejar.
    - Um usuário só pode ter no máximo uma recarga em andamento
- [x] Documente o seu projeto, e explique como rodar ele
- [x] Crie o projeto em algum repositório privado no GitHub ou GitLab

# Extras

- [ ] Adicione testes usando [Jest] ou qualquer outro framework para testes
- [ ] Usar Typescript
- [x] Coloque um docker-compose, que simplifique rodar o seu servidor e o DB
- [x] Usamos [prisma](prisma.io) mas sinta-se livre para usar qualquer ORM
- [ ] Adicione autenticação (apenas um usuário autenticado poderá fazer uma recarga ou uma reserva)
- [ ] Crie uma mutation `reservation`, que dado uma estação, um usuário e um intervalo de tempo, cria uma reserva da estação para o usuário naquele determinado intervalo de tempo.
  - Não deve ser possível criar uma reserva que conflite com o intervalo de outra reserva ou de uma recarga já em andamento
  - Para realizar uma recarga de uma determinada reserva, é necessário chamar uma mutation (podendo ser a própria `recharge` ou uma nova mutation - como você preferir) passando apenas um `reservationId`. A utilização só pode ocorrer dentro do próprio intervalo de tempo da reserva (e.g. Se a reserva foi de 12:00 até 13:00, só deve ser possível utilizá-la entre 12:00 e 13:00).
  - A recarga de uma reserva deve ser finalizada ao final do intervalo da reserva.
- [ ] Crie uma query `stationHistory`, onde será possível visualizar o histórico de recargas de uma estação (mostrar o horário, o tempo de duração da recarga e o usuário que realizou-a)

## Exemplo do dado da API da NASA

```json
[
  {
    "pl_hostname":"11 UMi",
    ...
    "pl_bmassj":14.74000,
    "pl_bmassjerr1":2.50000,
    "pl_bmassjerr2":-2.50000,
    "pl_bmassjlim":0,
    ...
  },
  ...
]
```

## Exemplo do dado da sua API

Dado uma query

```graphql
{
  suitablePlanets {
    name
    mass
    hasStation
  }
}
```

Retornar uma response

```json
{
    "suitablePlanets": [
        {
            "name": "XPTO",
            "mass": 27.5,
            "hasStation": false
        },
        {
            "name": "REPOLHO",
            "mass": 52.0,
            "hasStation": true
        },
        ...
    ]
}
```

## Exemplo das mutations

```graphql
{
  installStation(
    input: { name: "nome de exemplo", planet: "planeta de exemplo" }
  )
}
```

[nasa]: https://exoplanetarchive.ipac.caltech.edu/TAP/sync?query=select+*+from+ps&format=json
[jest]: https://jest-everywhere.now.sh/
[voltbras]: https://voltbras.com.br
[jupiter mass]: https://en.wikipedia.org/wiki/Jupiter_mass